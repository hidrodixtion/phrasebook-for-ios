## Phrasebook For iOS Project

### How to build :
1. Install cocoapod.
2. Install carthage.
3. Run `pod repo update` to update Cocoapod repository.
4. Run `pod install`.
5. Run `carthage update --platform iOS`.
6. Open **Phrasebook.xcworkspace** , remember, the **workspace**, not xcproject.
7. Build project in XCode.
8. Run.