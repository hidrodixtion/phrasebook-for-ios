//
//  IAPCell.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 12/22/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit

class IAPCell: UITableViewCell {

    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtDescription: UILabel!
    @IBOutlet weak var btnBuy: UIButton!    
    @IBOutlet weak var txtPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
