//
//  DBOps.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 11/8/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class DBOps {
    static let instance = DBOps()
    
    let realm = try! Realm()
    
    /**
     Fill realm database with raw data
    */
    func updateDataDb(data: [JSON]) {
        for item in data {
            let content = Content()
            content.id = item["id"].intValue
            content.title = item["title"].stringValue
            content.icon = item["icon"].stringValue
            
            if let free = item["free"].bool {
                content.free = free
            }
            
            for sub in item["subcontents"].arrayValue {
                let subcontent = SubContent()
                subcontent.id = sub["id"].stringValue
                subcontent.title = sub["title"].stringValue
                
                for phr in sub["phrases"].arrayValue {
                    let phrase = Phrase()
                    phrase.id = phr["id"].stringValue
                    phrase.english = phr["english"].stringValue
                    phrase.translate = phr["translate"].stringValue
                    phrase.romaji = phr["romaji"].string
                    phrase.sound = phr["sound"].stringValue
                    
                    phrase.free = content.free
                    
                    //print(phrase.free)
                    
                    subcontent.phrases.append(phrase)
                }
                
                content.subContents.append(subcontent)
            }
            
            try! realm.write {
                realm.add(content, update: true)
            }
        }
    }
    
    /**
     Update phrase favorite value to true / false
    */
    func updateFavorite(for phrase: Phrase) {
        try! realm.write {
             phrase.favorite = !phrase.favorite
        }
    }
    
    /**
     Change free status to true after user bought the iap
    */
    func updateFreeStatus(for contentTitle: String) {
        let contentResult = realm.objects(Content.self).filter("title ==[c] '\(contentTitle)'")
        
        if let content = contentResult.first {
            try! realm.write {
                content.free = true
            }
            
            for subcontent in content.subContents {
                for phrase in subcontent.phrases {
                    try! realm.write {
                        phrase.free = true
                    }
                }
            }
            
            //try! realm.write {
            //    realm.add(content, update: true)
            //}
        }
    }
}
