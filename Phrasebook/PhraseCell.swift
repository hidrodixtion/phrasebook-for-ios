//
//  StackCell.swift
//  Phrasebook
//
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit

class PhraseCell: UITableViewCell {

    @IBOutlet weak var txtEnglish: UILabel!
    @IBOutlet weak var txtRomaji: UILabel!
    @IBOutlet weak var txtTranslation: UILabel!
    @IBOutlet weak var expandedView: UIStackView!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var imgIndicator: UIImageView!
    @IBOutlet weak var lblFavInfo: UILabel!
    
    var isFolded: Bool = true {
        didSet {
            expandedView.isHidden = isFolded
        }
    }
    var phraseInCell: PhraseInCell?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnPlay._tintButtonImage(color: ColorName.blueTitle.color as UIColor)
        lblFavInfo.alpha = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setLabels(english: String, translation: String, romaji: String?) {
        txtEnglish.text = english
        txtTranslation.text = translation
        
        txtRomaji.isHidden = true
        
        if let romaji = romaji {
            txtRomaji.text = romaji
            txtRomaji.isHidden = false
        }
    }
    
    func setPhraseInCell(value: PhraseInCell) {
        phraseInCell = value
        
        guard let pIC = phraseInCell else {
            return
        }
        
        setLabels(english: pIC.phrase.english, translation: pIC.phrase.translate, romaji: pIC.phrase.romaji)
        
        updateFavButton(isFav: pIC.phrase.favorite)
        
        isFolded = pIC.isFolded
        
        UIView.animate(withDuration: 0.3) { [unowned self] in
            if (pIC.isFolded) {
                self.imgIndicator.transform = CGAffineTransform(rotationAngle: 0.0)
            } else {
                self.imgIndicator.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_2))
            }
        }
        
        // -------
        // if this cell is the result of search
        guard let searchedType = pIC.searchedType
            , let title = pIC.title
            , let range = pIC.searchedRange else {
            return
        }
        
        let attr = NSMutableAttributedString(string: title)
        attr.addAttribute(NSBackgroundColorAttributeName, value: UIColor(hex: "#FFEB3B"), range: range)
        
        switch searchedType {
        case .english:
            txtEnglish.attributedText = attr
        case .translation:
            txtTranslation.attributedText = attr
        case .romaji:
            txtRomaji.attributedText = attr
        }
    }
    
    func updateFavButton(isFav: Bool) {
        let image = isFav ? #imageLiteral(resourceName: "ic_like_full") : #imageLiteral(resourceName: "ic_like_empty")
        btnFavorite.setImage(image, for: .normal)
        btnFavorite._tintButtonImage(color: ColorName.blueTitle.color as UIColor)
    }
    
    @IBAction func onBtnFavClicked(_ sender: UIButton) {
        guard let pIC = phraseInCell else {
            return
        }
        
        DBOps.instance.updateFavorite(for: pIC.phrase)
        
        updateFavButton(isFav: pIC.phrase.favorite)
        
        lblFavInfo.text = pIC.phrase.favorite ? "Phrase added to favorites" : "Phrase removed from favorites"
        fadeIn()
    }
    
    private func fadeIn() {
        UIView.animate(withDuration: 0.3
            , animations: { [unowned self] in
                self.lblFavInfo.alpha = 1
            }, completion: { [unowned self] _ in
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                    self?.fadeOut()
                }
        })
    }
    
    private func fadeOut() {
        UIView.animate(withDuration: 0.3) { [unowned self] in
            self.lblFavInfo.alpha = 0
        }
    }
}
