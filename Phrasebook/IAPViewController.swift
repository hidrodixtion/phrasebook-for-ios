//
//  IAPViewController.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 12/22/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit
import StoreKit
import SwiftyStoreKit
import SwiftMessages

class IAPViewController: UITableViewController {

    let iapHelper = IAPHelper.instance
    
    var status: MessageView?
    var statusConfig: SwiftMessages.Config?
    
    var products = [SKProduct]()
    
    var isRestoring = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //iapHelper.updatePurchasedStatus()
        
        tableView.estimatedRowHeight = 72
        tableView.rowHeight = UITableViewAutomaticDimension
        
        updateProductsAndReload()
        
        initMessage()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Restore", style: .plain, target: self, action: #selector(onRestoreClicked))
        
        if iapHelper.products.count == 0 {
            status?.configureContent(body: "Retrieving in-app purchase list")
            SwiftMessages.show(config: statusConfig!, view: status!)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                self?.checkProduct()
            }
        }
        
        title = "Purchase"
    }
    
    /**
     Update products so it doesn't contain any purchased product
    */
    func updateProductsAndReload() {
        products = Array(iapHelper.products)
        
        guard let purchases = UserDefaults.standard.value(forKey: "purchased") as? [String] else {
            return
        }
        
        var unpurchasedProducts = [SKProduct]()
        for product in products {
            let index = purchases.index {
                $0 == product.productIdentifier
            }
            
            if index == nil {
                unpurchasedProducts.append(product)
            }
        }
        
        products = unpurchasedProducts
        tableView.reloadData()
    }
    
    /**
     Init SwiftMessages
    */
    func initMessage() {
        status = MessageView.viewFromNib(layout: .StatusLine)
        status?.backgroundView.backgroundColor = UIColor.white
        status?.bodyLabel?.textColor = ColorName.blueTitle.color
        // status?.configureContent(body: "Retrieving in-app purchase list")
        
        statusConfig = SwiftMessages.defaultConfig
        statusConfig?.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        statusConfig?.duration = .forever
    }
    
    /**
     Action for restore bar button item
    */
    func onRestoreClicked() {
        isRestoring = true
        
        guard let restoreButton = navigationItem.rightBarButtonItem else {
            isRestoring = false
            return
        }
        
        restoreButton.action = nil
        
        // show a message
        status?.configureContent(body: "Restoring purchase")
        SwiftMessages.show(config: statusConfig!, view: status!)
        
        iapHelper.restorePurchasedProducts { [weak self] in
            SwiftMessages.hideAll()
            
            guard let this = self else {
                return
            }
            
            restoreButton.action = #selector(this.onRestoreClicked)
            this.updateProductsAndReload()
            this.isRestoring = false
        }
    }
    
    /**
     Loop check the product until product information is returned by store
    */
    func checkProduct() {
        products = Array(iapHelper.products)
        
        if products.count == 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                self?.checkProduct()
            }
        } else {
            SwiftMessages.hideAll()
            
            updateProductsAndReload()
            tableView.reloadData()
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "IAPCell", for: indexPath) as! IAPCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "IAPCard", for: indexPath) as! IAPCard

        let product = products[indexPath.row]
        
        cell.txtTitle.text = product.localizedTitle
        cell.txtDescription.text = product.localizedDescription
        cell.txtPrice.text = product.localizedPrice
        
        cell.btnBuy.tag = indexPath.row
        cell.btnBuy.addTarget(self, action: #selector(buyProduct), for: .touchUpInside)

        return cell
    }
    
    /**
     Action when user tap buy button
    */
    func buyProduct(sender: UIButton) {
        if isRestoring {
            return
        }
        
        let product = products[sender.tag]
        
        // show a message
        status?.configureContent(body: "Connecting to iTunes Store")
        SwiftMessages.show(config: statusConfig!, view: status!)
        
        // remove action so user won't accidentally press buy twice
        sender.removeTarget(self, action: #selector(buyProduct), for: .touchUpInside)
        
        iapHelper.purchase(product: product.productIdentifier) { [weak self] in
            SwiftMessages.hideAll()
            sender.addTarget(self, action: #selector(self?.buyProduct), for: .touchUpInside)
        }
    }
}
