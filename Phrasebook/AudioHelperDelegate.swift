//
//  AudioHelperDelegate.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 11/23/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import Foundation

protocol AudioHelperDelegate: NSObjectProtocol {
    func audio(didFinishPlaying sound: String)
}
