//
//  IAPHelper.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 12/22/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import Foundation
import SwiftyJSON
import SwiftyStoreKit
import StoreKit

class IAPHelper {
    static let instance = IAPHelper()
    
    var iaps = [IAP]()
    var products = Set<SKProduct>()
    
    /**
     Read IAP raw data and convert into IAP object(s)
    */
    func readIAPData() {
        guard let data = JSONHelper.loadJSON("iap") else {
            print("can't read iap raw data")
            return
        }
        
        guard let iapArray = data.array else {
            return
        }
        
        iaps.removeAll()
        for item in iapArray {
            let unlocks = item["unlock"].arrayValue
            let items = unlocks.map { $0.stringValue }
            
            let iap = IAP(id: item["id"].stringValue, unlock: items, icon: item["icon"].string)
            iaps.append(iap)
        }
        
        retrieveProductsInfo() {}
        
        //print(UserDefaults.standard.value(forKey: "purchased"))
    }
    
    /**
     Get products info from converted iap(s)
    */
    func retrieveProductsInfo(completed: @escaping () -> ()) {
        // convert iap to set
        let iapSet = Set(iaps.map {$0.id})
        
        SwiftyStoreKit.retrieveProductsInfo(iapSet) { result in
            self.products = result.retrievedProducts
            self.sortProducts()
            completed()
        }
    }
    
    public func updatePurchasedStatus() {
        guard let purchases = UserDefaults.standard.value(forKey: "purchased") as? [String] else {
            return
        }
        
        for purchase in purchases {
            let index = iaps.index {
                $0.id == purchase
            }
            
            if let index = index {
                iaps[index].isPurchased = true
            }
        }
    }
    
    /**
     Restore purchased products and for every restored product, process the unlock
    */
    public func restorePurchasedProducts(completion: @escaping () -> ()) {
        var arrPurchased = [String]()
        
        SwiftyStoreKit.restorePurchases { results in
            if results.restoredProducts.count > 0 {
                for product in results.restoredProducts {
                    arrPurchased.append(product.productId)
                    
                    self.processPurchase(product: product)
                }
                
                UserDefaults.standard.set(arrPurchased, forKey: "purchased")
                UserDefaults.standard.synchronize()
                
//                print("ArrPurchased: \(arrPurchased)")
            }
            
            completion()
        }
    }
    
    /**
     Purchase a product
    */
    public func purchase(product id: String, completion: @escaping () -> ()) {
        SwiftyStoreKit.purchaseProduct(id) { [weak self] result in
            switch result {
            case .success(let purchased):
                self?.processPurchase(product: purchased)
            case .error(let error):
                print("Transaction failed with error: \(error)")
            }
            
            completion()
        }
    }
    
    /**
     Process purchase of product (process the unlock property)
    */
    private func processPurchase(product: Product) {
        let filteredIaps = iaps.filter { $0.id == product.productId }
        
        guard let iap = filteredIaps.first else {
            return
        }
        
        for item in iap.unlock {
            if item.lowercased() == "ads" {
                UserDefaults.standard.set(false, forKey: "show_ads")
            } else {
                DBOps.instance.updateFreeStatus(for: item)
            }
        }
    }
    
    /**
     Sort products according to iap order
    */
    public func sortProducts() {
        var skProducts = [SKProduct]()
        
        for iap in iaps {
            let index = products.index {
                $0.productIdentifier == iap.id
            }
            
            /*print("---")
            print(iap)
            print(products[index!].localizedTitle)*/
            
            skProducts.append(products[index!])
        }
        
        products = Set(skProducts)
    }
}
