//
//  JSONHelper.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 9/6/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import Foundation
import SwiftyJSON

class JSONHelper {
    static func loadJSON(_ filename: String) -> JSON? {
        if let path = Bundle.main.path(forResource: filename, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .dataReadingMapped)
                return JSON(data: data)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
