//
//  IAPGoods.swift
//  Phrasebook
//
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import Foundation

class IAP {
    var id: String
    var unlock: [String]
    var icon: String?
    var isPurchased: Bool = false
    
    init(id: String, unlock: [String], icon: String?) {
        self.id = id
        self.unlock = unlock
        self.icon = icon
    }
}
