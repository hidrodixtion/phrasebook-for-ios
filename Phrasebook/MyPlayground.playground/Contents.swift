//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
var arr = [1,5,6,3,9,0,3]
let myindex = arr.index {
    $0 == 4
}
arr.sort()
abs(-3)
arr[arr.count - 1] - arr[0]

var col = "F0F"
for (index, char) in col.characters.enumerated() {
    col.insert(char, at: col.index(col.startIndex, offsetBy: index * 2))
}
print(col)

var arr2 = [Int](0..<4)

class Ops {
    static let instance = Ops()
    
    var x = 9
}

let s = Ops.instance
s.x
s.x += 3

let t = Ops.instance
t.x

struct Playback {
    var label: String
    var rate: Float
}

let playbacks = [
    Playback(label: "Slowest", rate: 0.5),
    Playback(label: "Slow", rate: 0.75),
    Playback(label: "Normal", rate: 1),
    Playback(label: "Fast", rate: 1.25),
    Playback(label: "Fastest", rate: 1.5)
]

let index = playbacks.index {
    $0.rate == 1
}

print(index!.self)
