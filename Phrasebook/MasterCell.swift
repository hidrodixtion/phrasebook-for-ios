//
//  TableViewCell.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 9/28/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit

class MasterCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.image = Asset.icAllOut.image
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
