//
//  AudioHelper.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 11/23/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import AVFoundation
import UIKit

class AudioHelper: NSObject {
    static let instance: AudioHelper = AudioHelper()
    
    var player: AVAudioPlayer?
    weak open var audioDelegate: AudioHelperDelegate?
    var currentFilename = ""
    
    func playSound(filename: String, finishReceiver: AudioHelperDelegate? = nil) {
        guard let sound = NSDataAsset(name: filename) else {
            print("url not found")
            return
        }
        
        currentFilename = filename
        if let receiver = finishReceiver {
            audioDelegate = receiver
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(data: sound.data, fileTypeHint: AVFileTypeMPEGLayer3)
            player?.enableRate = true
            if let rate = UserDefaults.standard.value(forKeyPath: "playback_speed") as? Float {
                player?.rate = rate
            }
            player!.play()
            
            if player?.delegate == nil {
                player?.delegate = self
            }
        } catch let err as NSError {
            print(err.localizedDescription)
        }
    }
    
    func stopPlayer() {
        if let player = player {
            player.stop()
        }
    }
}

extension AudioHelper: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if let delegate = audioDelegate {
            delegate.audio(didFinishPlaying: currentFilename)
        }
    }
}
