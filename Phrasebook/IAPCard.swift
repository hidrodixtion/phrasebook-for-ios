//
//  IAPCard.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 12/23/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit

class IAPCard: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtDescription: UILabel!
    @IBOutlet weak var txtPrice: UILabel!
    @IBOutlet weak var btnBuy: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bgView.layer.cornerRadius = 4
        bgView.layer.masksToBounds = false
        bgView.layer.shadowColor = UIColor.black.cgColor
        bgView.layer.shadowOffset = CGSize(width: 1.1, height: 1.1)
        bgView.layer.shadowOpacity = 0.35
        bgView.layer.shadowRadius = 5
        
        imgIcon.image = #imageLiteral(resourceName: "ic_cart")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
