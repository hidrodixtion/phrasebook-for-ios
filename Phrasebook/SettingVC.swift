//
//  SettingVC.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 12/7/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit

class SettingVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Setting"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let rateDesc = UserDefaults.standard.value(forKeyPath: "playback_speed_desc") as? String {
            // get playback speed cell
            let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 1))
            cell?.detailTextLabel?.text = rateDesc
        }
        
        if let isOneExpand = UserDefaults.standard.value(forKeyPath: IS_ONLY_ONE_EXPAND) as? Bool {
            let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! SwitchCell
            cell.viewSwitch.isOn = isOneExpand
        }
        
        if let isAutoplay = UserDefaults.standard.value(forKeyPath: IS_AUTOPLAY) as? Bool {
            let cell = tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! SwitchCell
            cell.viewSwitch.isOn = isAutoplay
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 1
        default:
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Phrase"
        case 1:
            return "Audio"
        default:
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell: SwitchCell
            if indexPath.row == 0 {
                cell = tableView.dequeueReusableCell(withIdentifier: "CellSwitch", for: indexPath) as! SwitchCell
                cell.txtTitle.text = "Toggle expand"
                cell.txtSubtitle.text = "Expand one row at a time"
                cell.settingKey = IS_ONLY_ONE_EXPAND
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "CellSwitch", for: indexPath) as! SwitchCell
                cell.txtTitle.text = "Autoplay"
                cell.txtSubtitle.text = "Play phrase audio when selected"
                cell.settingKey = IS_AUTOPLAY
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellPick", for: indexPath)
            cell.textLabel?.text = "Playback speed"
//            cell.detailTextLabel?.text = "Normal"
            return cell
        default:
            let cell = UITableViewCell()
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 65
        default:
            return 44
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == StoryboardSegue.Main.segPlayback.rawValue {
            let vc = segue.destination as! PlaybackVC
            vc.title = "Playback Speed"
        }
    }
}
