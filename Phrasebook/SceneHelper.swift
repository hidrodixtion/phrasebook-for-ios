// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

import Foundation
import UIKit

// swiftlint:disable file_length
// swiftlint:disable line_length
// swiftlint:disable type_body_length

protocol StoryboardSceneType {
  static var storyboardName: String { get }
}

extension StoryboardSceneType {
  static func storyboard() -> UIStoryboard {
    return UIStoryboard(name: self.storyboardName, bundle: nil)
  }

  static func initialViewController() -> UIViewController {
    guard let vc = storyboard().instantiateInitialViewController() else {
      fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
    }
    return vc
  }
}

extension StoryboardSceneType where Self: RawRepresentable, Self.RawValue == String {
  func viewController() -> UIViewController {
    return Self.storyboard().instantiateViewController(withIdentifier: self.rawValue)
  }
  static func viewController(identifier: Self) -> UIViewController {
    return identifier.viewController()
  }
}

protocol StoryboardSegueType: RawRepresentable { }

extension UIViewController {
  func perform<S: StoryboardSegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
    performSegue(withIdentifier: segue.rawValue, sender: sender)
  }
}

struct StoryboardScene {
  enum IAP: String, StoryboardSceneType {
    static let storyboardName = "IAP"

    case vcAppPurchaseScene = "VCAppPurchase"
    static func instantiateVcAppPurchase() -> IAPViewController {
      guard let vc = StoryboardScene.IAP.vcAppPurchaseScene.viewController() as? IAPViewController
      else {
        fatalError("ViewController 'VCAppPurchase' is not of the expected class IAPViewController.")
      }
      return vc
    }

    case vcNavIAPScene = "VCNavIAP"
    static func instantiateVcNavIAP() -> UINavigationController {
      guard let vc = StoryboardScene.IAP.vcNavIAPScene.viewController() as? UINavigationController
      else {
        fatalError("ViewController 'VCNavIAP' is not of the expected class UINavigationController.")
      }
      return vc
    }
  }
  enum LaunchScreen: StoryboardSceneType {
    static let storyboardName = "LaunchScreen"
  }
  enum Main: String, StoryboardSceneType {
    static let storyboardName = "Main"

    static func initialViewController() -> UINavigationController {
      guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }

    case detailTableViewControllerScene = "DetailTableViewController"
    static func instantiateDetailTableViewController() -> DetailTableViewController {
      guard let vc = StoryboardScene.Main.detailTableViewControllerScene.viewController() as? DetailTableViewController
      else {
        fatalError("ViewController 'DetailTableViewController' is not of the expected class DetailTableViewController.")
      }
      return vc
    }

    case detailVCScene = "DetailVC"
    static func instantiateDetailVC() -> DetailViewController {
      guard let vc = StoryboardScene.Main.detailVCScene.viewController() as? DetailViewController
      else {
        fatalError("ViewController 'DetailVC' is not of the expected class DetailViewController.")
      }
      return vc
    }

    case masterVCScene = "MasterVC"
    static func instantiateMasterVC() -> MasterViewController {
      guard let vc = StoryboardScene.Main.masterVCScene.viewController() as? MasterViewController
      else {
        fatalError("ViewController 'MasterVC' is not of the expected class MasterViewController.")
      }
      return vc
    }

    case navDetailVCScene = "NavDetailVC"
    static func instantiateNavDetailVC() -> UINavigationController {
      guard let vc = StoryboardScene.Main.navDetailVCScene.viewController() as? UINavigationController
      else {
        fatalError("ViewController 'NavDetailVC' is not of the expected class UINavigationController.")
      }
      return vc
    }

    case settingVCScene = "SettingVC"
    static func instantiateSettingVC() -> SettingVC {
      guard let vc = StoryboardScene.Main.settingVCScene.viewController() as? SettingVC
      else {
        fatalError("ViewController 'SettingVC' is not of the expected class SettingVC.")
      }
      return vc
    }
  }
  enum Search: String, StoryboardSceneType {
    static let storyboardName = "Search"

    case navSearchVCScene = "NavSearchVC"
    static func instantiateNavSearchVC() -> UINavigationController {
      guard let vc = StoryboardScene.Search.navSearchVCScene.viewController() as? UINavigationController
      else {
        fatalError("ViewController 'NavSearchVC' is not of the expected class UINavigationController.")
      }
      return vc
    }

    case searchVCScene = "SearchVC"
    static func instantiateSearchVC() -> SearchViewController {
      guard let vc = StoryboardScene.Search.searchVCScene.viewController() as? SearchViewController
      else {
        fatalError("ViewController 'SearchVC' is not of the expected class SearchViewController.")
      }
      return vc
    }
  }
}

struct StoryboardSegue {
  enum Main: String, StoryboardSegueType {
    case segPlayback = "SegPlayback"
  }
}
