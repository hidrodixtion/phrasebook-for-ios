//
//  Content.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 11/7/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import Foundation
import RealmSwift

class Content: Object {
    dynamic var id: Int = 0
    dynamic var title: String = ""
    dynamic var free: Bool = false
    dynamic var icon: String = ""
    let subContents = List<SubContent>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
