// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

#if os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  typealias Color = UIColor
#elseif os(OSX)
  import AppKit.NSColor
  typealias Color = NSColor
#endif

extension Color {
  convenience init(rgbaValue: UInt32) {
    let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
    let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
    let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
    let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0

    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
}

// swiftlint:disable file_length
// swiftlint:disable type_body_length
enum ColorName {
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#2d8ee0"></span>
  /// Alpha: 100% <br/> (0x2d8ee0ff)
  case blueTitle
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#eaebec"></span>
  /// Alpha: 100% <br/> (0xeaebecff)
  case grayCell
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#f2f3f4"></span>
  /// Alpha: 100% <br/> (0xf2f3f4ff)
  case grayCellBg

  var rgbaValue: UInt32 {
    switch self {
    case .blueTitle: return 0x2d8ee0ff
    case .grayCell: return 0xeaebecff
    case .grayCellBg: return 0xf2f3f4ff
    }
  }

  var color: Color {
    return Color(named: self)
  }
}
// swiftlint:enable type_body_length

extension Color {
  convenience init(named name: ColorName) {
    self.init(rgbaValue: name.rgbaValue)
  }
}
