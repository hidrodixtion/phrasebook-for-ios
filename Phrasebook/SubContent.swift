//
//  SubContent.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 11/7/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import Foundation
import RealmSwift

class SubContent: Object {
    dynamic var id: String = ""
    dynamic var title: String = ""
    let phrases = List<Phrase>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
