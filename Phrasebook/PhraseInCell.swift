//
//  PhraseInCell.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 11/20/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import Foundation

/**
 This class will be used as representation for phrase in Cell. Each instance will have information about phrase cell.
 including :
    - isFolded: whether the cell is folded (true) or expanded (false)
    - phrase: the data / phrase for that cell
    
 and optional search information :
    - searchedRange: range for searched text
    - title: phrase title
    - searchedType: type for search (english / translation (& romaji))
 */
class PhraseInCell {
    var isFolded: Bool = true
    var phrase: Phrase = Phrase()
    
    var searchedRange: NSRange?
    var title: String?
    var searchedType: SearchType?
    
    init(isFolded: Bool, phrase: Phrase) {
        self.isFolded = isFolded
        self.phrase = phrase
    }
}
