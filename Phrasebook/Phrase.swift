//
//  Phrase.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 11/6/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import Foundation
import RealmSwift

class Phrase: Object {
    dynamic var id: String = ""
    dynamic var english: String = ""
    dynamic var translate: String = ""
    dynamic var romaji: String?
    dynamic var sound: String = ""
    dynamic var favorite: Bool = false
    dynamic var favoriteDate: Date = Date()
    dynamic var free: Bool = true
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
