//
//  MasterViewController.swift
//  Phrasebook
//
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import SwiftMessages
import Appodeal

class MasterViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewFavorites: UIStackView!
    @IBOutlet weak var viewSearch: UIStackView!
    @IBOutlet weak var viewPurchase: UIStackView!
    @IBOutlet weak var viewSetting: UIStackView!
    
    var contents: Results<Content>?
    let realm = try! Realm()
    
    var isFirstTimeOpened = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViewTaps();
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 93
        
        tableView.dataSource = self
        tableView.delegate = self
        
//        print(realm.objects(Content.self).filter("title ==[c] 'grEETings'"))
        
        customizeBackNav()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SwiftMessages.hideAll()
        
        contents = realm.objects(Content.self).filter("free = true")
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // if user hasn't buy 'Remove Ads' IAP, show interstitial for every defined value
        // show the interstitial at every defined value in setting & when not in the first time user open app
        if (UserDefaults.standard.bool(forKey: "show_ads")) {
            
            Setting.inters_ad_count += 1
            
            if (Setting.inters_ad_count % Setting.show_interstitial_every) == 0 {
                
                if !isFirstTimeOpened {
                    Appodeal.showAd(.interstitial, rootViewController: self)
                }
                
                isFirstTimeOpened = false
            }
        }
    }
    
    var state: Int = 0
    
    /**
     Make back navigation without "Back" title
    */
    func customizeBackNav() {
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButtonItem
    }
    
    func initViewTaps() {
        let favTaps = UITapGestureRecognizer(target: self, action: #selector(onFavoritesClicked))
        viewFavorites.addGestureRecognizer(favTaps)
        
        let searchTaps = UITapGestureRecognizer(target: self, action: #selector(onSearchClicked))
        viewSearch.addGestureRecognizer(searchTaps)
        
        let settingTaps = UITapGestureRecognizer(target: self, action: #selector(onSettingClicked))
        viewSetting.addGestureRecognizer(settingTaps)
        
        let purchaseTaps = UITapGestureRecognizer(target: self, action: #selector(onPurchaseClicked))
        viewPurchase.addGestureRecognizer(purchaseTaps)
    }
    
    // MARK: - ACTION
    
    // action favorites
    func onFavoritesClicked(recognizer: UITapGestureRecognizer) {
        
        guard let nav = self.navigationController else {
            return
        }
        
        let vc = StoryboardScene.Main.instantiateDetailTableViewController()
        
        vc.title = "Favorites"
        
        let phrases = List<Phrase>()
        
        let predicate = NSPredicate(format: "favorite == %@", argumentArray: [true])
        let result = realm.objects(Phrase.self).filter(predicate)
        
        for phrase in result {
            phrases.append(phrase)
        }
        
        vc.phrases = phrases
        
        nav.pushViewController(vc, animated: true)
    }
    
    // action search
    func onSearchClicked(recognizer: UITapGestureRecognizer) {
        let searchVC = StoryboardScene.Search.instantiateSearchVC()
        
        if let nav = self.navigationController {
            nav.pushViewController(searchVC, animated: true)
        }
    }
    
    // action setting
    func onSettingClicked(recognizer: UITapGestureRecognizer) {
        let settingVC = StoryboardScene.Main.instantiateSettingVC()
        
        if let nav = self.navigationController {
            nav.pushViewController(settingVC, animated: true)
        }
    }
    
    // action purchase
    func onPurchaseClicked(recognizer: UITapGestureRecognizer) {
        let iapVC = StoryboardScene.IAP.instantiateVcAppPurchase()
        
        if let nav = self.navigationController {
            nav.pushViewController(iapVC, animated: true)
        }
    }
}

extension MasterViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let contents = contents {
            return contents.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MasterCell", for: indexPath) as! MasterCell
        
        if let contents = contents {
            let content = contents[indexPath.row]
            cell.title.text = content.title
            
            if let icon = Asset(rawValue: content.icon) {
                cell.imgView.image = icon.image
            }
        }
        
        if (indexPath.row % 2 == 0) {
            cell.backgroundColor = ColorName.grayCell.color.withAlphaComponent(0.5)
        } else {
            cell.backgroundColor = UIColor.white
        }
        
        return cell
    }
}

extension MasterViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = StoryboardScene.Main.instantiateDetailVC()
        
        if let contents = contents {
            vc._data = contents[indexPath.row]
        }
        
        if let nav = self.navigationController {
            nav.pushViewController(vc, animated: true)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
