//
//  SearchViewController.swift
//  Phrasebook
//
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit
import RealmSwift

enum SearchType: String {
    case translation
    case english
    case romaji
}

class SearchViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var phrases: Results<Phrase>?
    var phrasesInCell = [PhraseInCell]()
    var stringToSearch = ""
    
    let searchController = UISearchController(searchResultsController: nil)
    let realm = try! Realm()
    
    var isAutoplay = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Search Phrase"
        
        if let autoplay = UserDefaults.standard.value(forKeyPath: "is_autoplay") as? Bool {
            isAutoplay = autoplay
        }
        
//        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "PhraseCell", bundle: nil), forCellReuseIdentifier: "PhraseCell")
        
        tableView.dataSource = self
        tableView.delegate = self
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.scopeButtonTitles = ["English", "Translation"]
        searchController.searchBar.delegate = self
        
        tableView.tableHeaderView = searchController.searchBar
    }
    
    /**
     *  Find phrase for given text
     */
    func searchPhrase(text: String, scope: SearchType) {
        stringToSearch = text
        if text == "" {
            phrases = nil
            tableView.reloadData()
            return
        }
        
        tableView.rowHeight = UITableViewAutomaticDimension
        
        switch scope {
        case .english:
            phrases = realm.objects(Phrase.self).filter("english CONTAINS[c] '\(text)' AND free == true")
        case .translation:
            phrases = realm.objects(Phrase.self).filter("(translate CONTAINS[c] '\(text)' OR romaji CONTAINS[c] '\(text)') AND free == true")
        default:
            break
        }
        createPhraseInCells(from: phrases, scope: scope)
        tableView.reloadData()
    }
    
    func createPhraseInCells(from phrases: Results<Phrase>?, scope: SearchType) {
        guard let phrases = phrases else {
            return
        }
        
        phrasesInCell.removeAll()
        
        let text = stringToSearch
        
        for phrase in phrases {
            let pic = PhraseInCell(isFolded: true, phrase: phrase)
            pic.searchedType = scope
            
            let nsrange: Range<String.Index>?
            
            switch scope {
            case .english:
                pic.title = phrase.english
                nsrange = phrase.english.lowercased().range(of: text.lowercased())
            case .translation:
                if let range = phrase.translate.lowercased().range(of: text.lowercased()) {
                    pic.title = phrase.translate
                    nsrange = range
                } else {
                    pic.title = phrase.romaji
                    nsrange = phrase.romaji!.lowercased().range(of: text.lowercased())
                    pic.searchedType = .romaji
                }
                pic.isFolded = false
            default:
                nsrange = nil
            }
            
            if let nsrange = nsrange {
                let start = pic.title!.distance(from: pic.title!.startIndex, to: nsrange.lowerBound)
                pic.searchedRange = NSMakeRange(start, text.characters.count)
            }
            
            phrasesInCell.append(pic)
        }

    }
}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let phrases = phrases {
            return phrases.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhraseCell", for: indexPath) as! PhraseCell
        
        let pIC = phrasesInCell[indexPath.row]
        cell.setPhraseInCell(value: pIC)
        cell.btnPlay.tag = indexPath.row
        cell.btnPlay.addTarget(self, action: #selector(onPlayClicked), for: .touchUpInside)
        
        return cell
    }
    
    /**
     _Selector_ for button play when clicked
     */
    func onPlayClicked(sender: UIButton) {
        guard let phrases = phrases else{
            return
        }
        
        let index = sender.tag
        let phrase = phrases[index]
        
        AudioHelper.instance.playSound(filename: phrase.sound)
    }
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pIC = phrasesInCell[indexPath.row]
        
        // only if autoplay, folded and searchType is english, then play the phrase
        if isAutoplay && pIC.isFolded && pIC.searchedType! == .english {
            AudioHelper.instance.playSound(filename: pIC.phrase.sound)
        }
        
        pIC.isFolded = !pIC.isFolded
        
        tableView.reloadRows(at: [indexPath], with: .automatic)
//        tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let pIC = phrasesInCell[indexPath.row]
        
        switch pIC.searchedType! {
        case .english:
            return 60
        case .translation:
            return 128
        case .romaji:
            return 150
        }
    }
}

extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let selectedScopeIndex = searchController.searchBar.selectedScopeButtonIndex
        let scope = SearchType(rawValue: searchController.searchBar.scopeButtonTitles![selectedScopeIndex].lowercased())!
        searchPhrase(text: searchController.searchBar.text!, scope: scope)
    }
}

// - MARK: UISearchBar DELEGATE
extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        let scope = SearchType(rawValue: searchController.searchBar.scopeButtonTitles![selectedScope].lowercased())!
        searchPhrase(text: searchBar.text!, scope: scope)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        AudioHelper.instance.stopPlayer()
    }
}
