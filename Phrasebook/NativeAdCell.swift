//
//  NativeAdCell.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 1/26/17.
//  Copyright © 2017 Adi Nugroho. All rights reserved.
//

import UIKit
import Appodeal

class NativeAdCell: UITableViewCell {

    var mediaView: APDMediaView!
    fileprivate var rootVC: UIViewController!
    
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgView.image = UIImage(named: Setting.native_ad_alt)
    }
    
    func initAd(apLoader: APDNativeAdLoader, rootVC: UIViewController) {
        self.rootVC = rootVC
        
        apLoader.delegate = self
        apLoader.loadAd(with: Setting.native_ad_mode)
//        if let ad = nativeAd {
//            ad.detachFromView()
//            
//            ad.attach(to: contentView, viewController: rootVC)
//            mediaView.setNativeAd(ad, rootViewController: rootVC)
//            
//            if let adChoices = ad.adChoicesView {
//                adChoices.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
//                contentView.addSubview(adChoices)
//            }
//        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension NativeAdCell: APDNativeAdLoaderDelegate {
    func nativeAdLoader(_ loader: APDNativeAdLoader!, didLoad nativeAd: APDNativeAd!) {
        imgView.image = nil
        nativeAd.detachFromView()
        nativeAd.attach(to: contentView, viewController: rootVC)
        
        mediaView = APDMediaView(frame: CGRect(x: 0, y: 0, width: contentView.bounds.size.width, height: contentView.bounds.size.height))
        mediaView.type = .mainImage
        mediaView.skippable = true
        mediaView.muted = false
        
        contentView.addSubview(mediaView)

        mediaView.setNativeAd(nativeAd, rootViewController: rootVC)
        
        if let adChoices = nativeAd.adChoicesView {
            adChoices.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
            contentView.addSubview(adChoices)
        }
    }
    
    func nativeAdLoader(_ loader: APDNativeAdLoader!, didFailToLoadWithError error: Error!) {
        loader.loadAd(with: Setting.native_ad_mode)
    }
}
