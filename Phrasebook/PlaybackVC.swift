//
//  PlaybackVC.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 12/7/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit

class PlaybackVC: UITableViewController {
    
    let playbacks = [
        Playback(label: "Slowest", rate: 0.5),
        Playback(label: "Slow", rate: 0.75),
        Playback(label: "Normal", rate: 1),
        Playback(label: "Fast", rate: 1.25),
        Playback(label: "Fastest", rate: 1.5)
    ]
    var lastSelectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // get speed from UserDefault and set the selectedIndex (this will be used in
        // cellForRowAt) to set the checkmark.
        if let rate = UserDefaults.standard.value(forKey: "playback_speed") as? Float {
            let index = playbacks.index {$0.rate == rate}
            lastSelectedIndex = index!
        }
    }
    
    func setSelected(speed: Float) {
        let index = playbacks.index {
            $0.rate == speed
        }
        
        let indexPath = IndexPath(row: index!, section: 0)
        showCheckMark(at: indexPath)
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playbacks.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CheckCell
        cell.txtTitle.text = playbacks[indexPath.row].label
        
        if indexPath.row == lastSelectedIndex {
            cell.imgCheck.isHidden = false
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // deselect last row
        let cell = tableView.cellForRow(at: IndexPath(row: lastSelectedIndex, section: 0)) as! CheckCell
        cell.imgCheck.isHidden = true
        
        showCheckMark(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    /**
     Get selected cell and show the checkmark
    */
    func showCheckMark(at index: IndexPath) {
        if let cell = tableView.cellForRow(at: index) as? CheckCell {
            cell.imgCheck.isHidden = false
        }
        lastSelectedIndex = index.row
        let playback = playbacks[lastSelectedIndex]
        UserDefaults.standard.set(playback.rate, forKey: "playback_speed")
        UserDefaults.standard.set(playback.label, forKey: "playback_speed_desc")
    }
}
