//
//  DetailTableViewController.swift
//  Phrasebook
//
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit
import AVFoundation
import SwiftyJSON
import RealmSwift
import Appodeal

class DetailTableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var player: AVAudioPlayer?
    var data: SubContent?
    var phrases: List<Phrase>?
    var phrasesInCell = [AnyObject]()
    var isSequence = false
    var playIndex = -1
    var selectedIndex: IndexPath?
    var isAutoplay: Bool = false
    var isOneExpand: Bool = true
    
    // ads vars
    var startAdIndex = Setting.start_show_native_at
    var adsEvery = Setting.show_native_every
    var adsToLoad = [APDNativeAdLoader]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let data = data {
            phrases = data.phrases
            
            createPhrasesInCell()
        } else if let _ = phrases {
            createPhrasesInCell()
        }
        
        if let autoplay = UserDefaults.standard.value(forKeyPath: "is_autoplay") as? Bool {
            isAutoplay = autoplay
        }
        
        if let oneExpand = UserDefaults.standard.value(forKeyPath: IS_ONLY_ONE_EXPAND) as? Bool {
            isOneExpand = oneExpand
        }
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        tableView.tableFooterView = UIView()
        
        tableView.register(UINib(nibName: "NativeAdCell", bundle: nil), forCellReuseIdentifier: "NativeAdCell")
        tableView.register(UINib(nibName: "PhraseCell", bundle: nil), forCellReuseIdentifier: "PhraseCell")
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func createPhrasesInCell() {
        phrasesInCell.removeAll()
        
        guard let phrases = phrases else {
            return
        }
        
        for phrase in phrases {
            phrasesInCell.append(PhraseInCell(isFolded: true, phrase: phrase))
        }
        
        // check if user have bought 'Remove Ads' IAP , if yes, do not add native ads in table
        if (!UserDefaults.standard.bool(forKey: "show_ads")) {
            return
        }
        
        // add a native ad in defined row & its multipy
        while startAdIndex < phrasesInCell.count {
            let apLoader = APDNativeAdLoader()
            phrasesInCell.insert(apLoader, at: startAdIndex)
            adsToLoad.append(apLoader)
            startAdIndex += adsEvery
        }
    }
}

extension DetailTableViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phrasesInCell.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // check if cell is phrase or ad
        if let pIC = phrasesInCell[indexPath.row] as? PhraseInCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PhraseCell", for: indexPath) as! PhraseCell
            
            cell.setPhraseInCell(value: pIC)
            cell.btnPlay.tag = indexPath.row
            cell.btnPlay.addTarget(self, action: #selector(onPlayClicked), for: .touchUpInside)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NativeAdCell", for: indexPath) as! NativeAdCell
            cell.initAd(apLoader: phrasesInCell[indexPath.row] as! APDNativeAdLoader, rootVC: self)
            return cell
        }
        
    }
    
    /**
     [Selector] When play button is tapped
     */
    func onPlayClicked(sender: UIButton) {
        let index = sender.tag
        
        isSequence = false
        playIndex = -1
        
        playSound(for: index)
    }
    
    /**
     Play phrase sound in sequence
    */
    func playSoundSequence() {
        isSequence = true

        if let _ = phrasesInCell[0] as? PhraseInCell {
            playIndex = 0
        } else {
            playIndex = 1
        }
        
        // check if last selectedIndex is expanded, fold it.
        for i in 0..<phrasesInCell.count {
            guard let phraseIC = phrasesInCell[i] as? PhraseInCell else {
                continue
            }
            
            phraseIC.isFolded = false
            let indexPath = IndexPath(row: i, section: 0)
            changeStackState(at: indexPath)
        }
        
        selectRowAndExpand(at: playIndex)
        
        playSound(for: playIndex)
    }
    
    /**
     Play phrase sound
    */
    fileprivate func playSound(for index: Int) {
        guard let _ = phrases else {
            print("no phrases")
            return
        }
        
        if index >= phrasesInCell.count {
            return
        }
        print("\(index) \(phrasesInCell.count)")
        
        if let phraseIC = phrasesInCell[index] as? PhraseInCell {
            print(phraseIC.phrase.english)
            AudioHelper.instance.playSound(filename: phraseIC.phrase.sound, finishReceiver: self)
        }
    }
}

// - MARK: UITableView DELEGATE
extension DetailTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let pIC = phrasesInCell[indexPath.row] as? PhraseInCell else {
            if (tableView.cellForRow(at: indexPath) as! NativeAdCell).imgView.image != nil {
                openPurchaseVC()
            }
            return
        }
        
        if isAutoplay && pIC.isFolded {
            let phrase = pIC.phrase
            AudioHelper.instance.playSound(filename: phrase.sound, finishReceiver: self)
        }
        
        if isOneExpand {
            if let selected = selectedIndex {
                if selected != indexPath {
                    changeStackState(at: selected)
                }
            }
        }
        
        changeStackState(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let _ = phrasesInCell[indexPath.row] as? APDNativeAdLoader {
            return CGFloat(Setting.native_ad_height)
        }
        
        return UITableViewAutomaticDimension
    }
    
    fileprivate func changeStackState(at indexPath: IndexPath) {
        selectedIndex = indexPath
        
        if let pIC = phrasesInCell[indexPath.row] as? PhraseInCell {
            pIC.isFolded = !pIC.isFolded
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    fileprivate func openPurchaseVC() {
        navigationController?.pushViewController(StoryboardScene.IAP.instantiateVcAppPurchase(), animated: true)
    }
}


// - MARK: AudioHelper DELEGATE
extension DetailTableViewController: AudioHelperDelegate {
    func audio(didFinishPlaying sound: String) {
        if isSequence {
            playIndex += 1
            
            if playIndex >= phrasesInCell.count {
                print("playIndex is exceeding phrases count")
                return
            }
            
            if let _ = phrasesInCell[playIndex] as? APDNativeAdLoader {
                playIndex += 1
            }
            
            if let selected = selectedIndex {
                changeStackState(at: selected)
            }
            
            selectRowAndExpand(at: playIndex)
            playSound(for: playIndex)
        }
    }
    
    func selectRowAndExpand(at index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        changeStackState(at: indexPath)
    }
}
