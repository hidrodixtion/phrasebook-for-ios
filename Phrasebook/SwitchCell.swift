//
//  SwitchCell.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 12/7/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit

class SwitchCell: UITableViewCell {

    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtSubtitle: UILabel!
    @IBOutlet weak var viewSwitch: UISwitch!
    
    var settingKey: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onSwitchChanged(_ sender: UISwitch) {
        guard let key = settingKey else {
            return
        }
        
        UserDefaults.standard.set(sender.isOn, forKey: key)
    }
}
