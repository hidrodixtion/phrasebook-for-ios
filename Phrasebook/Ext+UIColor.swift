//
//  Ext+UIColor.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 9/28/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit

public extension UIColor {
    convenience init(hex color: String) {
        var hex = color.hasPrefix("#") ? String(color.characters.dropFirst()) : color
        
        guard hex.characters.count == 3 || hex.characters.count == 6 else {
            self.init(white: 1.0, alpha: 0.0)
            return
        }
        
        if hex.characters.count == 3 {
            for (index, char) in hex.characters.enumerated() {
                hex.insert(char, at: hex.index(hex.startIndex, offsetBy: index * 2))
            }
        }
        
        self.init(
            red: CGFloat((Int(hex, radix: 16)! >> 16) & 0xFF) / 255.0,
            green: CGFloat((Int(hex, radix: 16)! >> 8) & 0xFF) / 255.0,
            blue: CGFloat(Int(hex, radix: 16)! & 0xFF) / 255.0,
            alpha: 1.0
        )
    }
}
