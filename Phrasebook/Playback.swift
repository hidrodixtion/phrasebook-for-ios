//
//  Playback.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 12/7/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import Foundation

struct Playback {
    var label: String
    var rate: Float
}
