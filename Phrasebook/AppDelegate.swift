//
//  AppDelegate.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 9/23/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyStoreKit
import Appodeal

// settings global var
let IS_ONLY_ONE_EXPAND = "isOnlyOneExpanded"
let IS_AUTOPLAY = "isAutoplay"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?
    
    override init() {
        super.init()
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Appodeal.setTestingEnabled(Setting.ad_test_mode)
        
        Appodeal.initialize(withApiKey: Setting.ad_id, types: [.interstitial, .nativeAd])
        
        APDSdk.shared().setLogLevel(Setting.ad_log_level)
        
        Appodeal.cacheAd(.interstitial)
        
        // customizing global styles
        customizeGlobal()
        
        // configure realm
        configRealm()
        
        // check build version
        checkBuildVersion()
        
        // init audio player, so next time it plays, there won't be any delay / pause before the sound is being played
        AudioHelper.instance.playSound(filename: "silent")
        
        // init storekit
        initStoreKit()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }

    func customizeGlobal() {
        UIApplication.shared.statusBarStyle = .lightContent
        
        let font = UIFont(name: "AvenirNext-Regular", size: 17)!
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: font], for: .normal)
        
        let smallfont = UIFont(name: "AvenirNext-Regular", size: 15)!
        
        UISearchBar.appearance().barTintColor = Color(named: ColorName.blueTitle)
        UISearchBar.appearance().tintColor = Color.white
        UISearchBar.appearance().setScopeBarButtonTitleTextAttributes([NSFontAttributeName: smallfont], for: .normal)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = Color(named: ColorName.blueTitle)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = smallfont
    }
    
    /**
     Check Build version and if the build version is different than what's saved, update the Realm db
    */
    func checkBuildVersion() {
        guard let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String else {
            print("can't get build version")
            return
        }
        
        if let bundle = UserDefaults.standard.value(forKey: "bundle_version") as? String {
            if bundle != build {
                updateRealmData()
            }
        } else {
            updateRealmData()
        }
        
        UserDefaults.standard.set(build, forKey: "bundle_version")
        
        checkSetting()
    }
    
    /**
     Check for User setting, if there's no recorded setting, create one
    */
    func checkSetting() {
        if let _ = UserDefaults.standard.value(forKey: "is_autoplay") as? Bool {
            return
        } else {
            UserDefaults.standard.set(false, forKey: IS_AUTOPLAY)
            UserDefaults.standard.set(true, forKey: IS_ONLY_ONE_EXPAND)
            UserDefaults.standard.set(1.0, forKey: "playback_speed")
            UserDefaults.standard.set("Normal", forKey: "playback_speed_desc")
            UserDefaults.standard.set(true, forKey: "show_ads")
        }
    }
    
    /**
     Load from data.json and update the Realm db
     */
    func updateRealmData() {
        guard let data = JSONHelper.loadJSON("data") else {
            print("Can't load data")
            return
        }
        
        DBOps.instance.updateDataDb(data: data.arrayValue)
    }
    
    /**
     Configure realm migration
     */
    func configRealm() {
        let config = Realm.Configuration(
            schemaVersion: 1,
            
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1) {
                    
                }
        })
        
        Realm.Configuration.defaultConfiguration = config
    }
    
    /**
     Init in app processing and finish any transaction that needs to be finished
    */
    func initStoreKit() {
        SwiftyStoreKit.completeTransactions { products in
            for product in products {
                if product.transaction.transactionState == .purchased || product.transaction.transactionState == .restored {
                    if product.needsFinishTransaction {
                        SwiftyStoreKit.finishTransaction(product.transaction)
                    }
                }
            }
        }
        
        if UserDefaults.standard.value(forKey: "purchased") == nil {
            IAPHelper.instance.restorePurchasedProducts {}
        }
        
        IAPHelper.instance.readIAPData()
    }
}

