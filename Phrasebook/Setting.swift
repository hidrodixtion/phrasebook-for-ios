//
//  Setting.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 12/8/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import Foundation
import Appodeal

struct Setting {
    
    // ads setting
    static let ad_id = "ec5bcc2ba4fd19794335a238e72aad46f3ee2722c7ae7ef1"
    static let ad_test_mode = true
    static let ad_log_level: APDLogLevel = .off
    
    static let show_interstitial_every = 5
    
    static let show_native_every = 8
    static let start_show_native_at = 0
    static let native_ad_mode: APDNativeAdType = .noVideo
    static let native_ad_height: Float = 100
    static let native_ad_alt: String = "remove_ads"
    
    // DO NOT change this to let / immutable. This info will be used as interstitial show counter
    static var inters_ad_count = -1
}
