//
//  PagingMenuOptions.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 10/3/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import Foundation
import PagingMenuController

class PagingMenuOption: PagingMenuControllerCustomizable {
    var _vcs = [UIViewController]()
    var _menuOptions: MenuView?
    
    var componentType: ComponentType {
        return .all(menuOptions: _menuOptions!, pagingControllers: _vcs)
    }
    
    init(controllers: [UIViewController], titles: [String]) {
        _vcs = controllers
        _menuOptions = MenuView(items: titles)
    }
}

class MenuTitle: MenuItemViewCustomizable {
    var _title: String = ""
    
    init(title: String) {
        _title = title
    }
    
    var displayMode: MenuItemDisplayMode {
        let font = UIFont.init(name: "AvenirNext-Bold", size: 17)!
        let itemText = MenuItemText(text: _title,
                                    color: UIColor.white.withAlphaComponent(0.5),
                                    selectedColor: UIColor.white,
                                    font: font,
                                    selectedFont: font)
        return .text(title: itemText)
    }
}

class MenuView: MenuViewCustomizable {
    var _itemViews = [MenuItemViewCustomizable]()
    var _itemCount = 1
    
    var backgroundColor: UIColor {
        return ColorName.blueTitle.color
    }
    
    var selectedBackgroundColor: UIColor {
        return ColorName.blueTitle.color
    }
    
    var focusMode: MenuFocusMode {
        return .underline(height: 4, color: UIColor.white, horizontalPadding: 0, verticalPadding: 0)
    }
    
    var itemsOptions: [MenuItemViewCustomizable] {
        return _itemViews
    }
    
    var displayMode: MenuDisplayMode {
        return .standard(widthMode: .flexible, centerItem: true, scrollingMode: .pagingEnabled)
    }
    
    var height: CGFloat {
        if (_itemCount == 1) {
            return 0.01
        }
        
        return 50
    }
    
    init(items: [String]) {
        _itemViews.removeAll()
        
        _itemCount = items.count
        
        for item in items {
            _itemViews.append(MenuTitle(title: item))
        }
    }
}

