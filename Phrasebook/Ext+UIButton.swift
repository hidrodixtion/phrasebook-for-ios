//
//  Ext+Button.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 9/28/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit

extension UIButton {
    
    func _tintButtonImage(hex: String) {
        let image = self.imageView?.image?.withRenderingMode(.alwaysTemplate)
        self.setImage(image, for: .normal)
        self.imageView?.tintColor = UIColor(hex: hex)
    }
    
    func _tintButtonImage(color: UIColor) {
        let image = self.imageView?.image?.withRenderingMode(.alwaysTemplate)
        self.setImage(image, for: .normal)
        self.imageView?.tintColor = color
    }
    
    func _tintButtonWhite() {
        let image = self.imageView?.image?.withRenderingMode(.alwaysTemplate)
        self.setImage(image, for: .normal)
        self.imageView?.tintColor = UIColor.white        
    }
}
