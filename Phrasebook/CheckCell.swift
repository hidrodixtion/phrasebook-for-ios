//
//  CheckCell.swift
//  Phrasebook
//
//  Created by Adi Nugroho on 12/8/16.
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit

class CheckCell: UITableViewCell {
    
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgCheck.tintColor = Color.init(named: .blueTitle)
        imgCheck.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
