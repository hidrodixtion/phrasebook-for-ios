//
//  DetailViewController.swift
//  Phrasebook
//
//  Copyright © 2016 Adi Nugroho. All rights reserved.
//

import UIKit
import PagingMenuController
import SwiftyJSON

class DetailViewController: UIViewController {
    
    /// data for selected title
    var _data: Content?
    var vcs = [DetailTableViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let data = _data else {
            return
        }
        
        self.title = data.title
        
        let btnBarPlayAll = UIBarButtonItem(title: "Play All", style: .plain, target: self, action: #selector(onPlayAllClicked))
        self.navigationItem.rightBarButtonItem = btnBarPlayAll
        
        if let pagingMenu = self.childViewControllers.first as? PagingMenuController {
            vcs.removeAll()            
            var menus = [String]()
            
            for content in data.subContents {
                let vc = StoryboardScene.Main.instantiateDetailTableViewController()
                vc.data = content
                vcs.append(vc)
                
                menus.append(content.title)
            }
            
            let option = PagingMenuOption(
                controllers: vcs,
                titles: menus)
            
            pagingMenu.setup(option)
            pagingMenu.onMove = { state in
                switch state {
                case .didMoveController(_, _):
                    AudioHelper.instance.stopPlayer()
                default:
                    break
                }
            }
        }
    }
    
    /**
     Play All button clicked listener
    */
    func onPlayAllClicked() {
        if let pagingMenu = self.childViewControllers.first as? PagingMenuController {
            let currentVC = vcs[pagingMenu.currentPage]
            
            currentVC.playSoundSequence()
        }
    }
    
    /**
     When this vc is disapper, and check if it's moving away from ParentVC
     * meaning that it's being removed from nav controller.
    */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParentViewController {
            AudioHelper.instance.stopPlayer()
        }
    }
}
